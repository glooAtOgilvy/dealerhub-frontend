<?php
/* Template Name: Home Page */

?>
<?php get_header();?>

    <!DOCTYPE html>
    <html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Google Tag Manager (noscript) -->
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M9C9LTM"
        height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->
    </head>
    <body class="content-index">


    <div class="landing_page hero">
        <img src="<?php echo get_template_directory_uri();?>/images/blank.png" style="visibility: hidden!important;">
    </div>
    <div class="outer-container">
        <?php get_sidebar(); ?>

        <div class="main-copy-holder content-copy">
            <!--Navigation-->
            <div class="page-menu">
                <div class="off-canvas-menu">
                    <ul id="menu-audi-e-tron-launch-campaign-2022" class="menu">
                        <li id="menu-item-8957" class="first menu-item menu-item-type-custom menu-item-object-custom menu-item-8957" style="display: none;"><a href="#menu">Menu</a></li>
                        <li id="menu-item-8958" class="menu-item menu-item-type-custom menu-item-object-custom"><a href="#home-intro">New Models</a></li>
                        <li id="menu-item-8958" class="menu-item menu-item-type-custom menu-item-object-custom"><a href="#pre-owned">Pre-owned</a></li>
                        <li id="menu-item-8958" class="menu-item menu-item-type-custom menu-item-object-custom"><a href="#archive">Archives</a></li>
                    </ul>
                </div>
            </div>

            <!--search bar-->
            <div class="search-bar-holder">
                <div class="search-bar">
                    <?php
                    while (have_posts()) :
                        the_post();
                        get_template_part('template-parts/content/content-page-search');
                    endwhile;
                    ?>
                </div>
            </div>
            <!--search bar end-->

            <!--main copy page intro-->
            <div id="home-intro" class="intro">
            <h3 class="page-tittle">Vehicle Campaigns</h3>
                <!-- <a href="/marketing-and-communications-new-models/">New Models</a>
                <a href="/marketing-and-communications-pre-owned/">Pre-owned</a> -->
            </div>

			<?php
				/* $args = array(
				 'show_option_all'    => '',
				 'orderby'            => 'name',
				 'order'              => 'ASC',
				 'style'              => 'list',
				 'show_count'         => 0,
				 'hide_empty'         => 1,
				 'use_desc_for_title' => 1,
				 'child_of'           => 0,
				 'feed'               => '',
				 'feed_type'          => '',
				 'feed_image'         => '',
				 'exclude'            => '',
				 'exclude_tree'       => '',
				 'include'            => '',
				 'hierarchical'       => 1,
				 'title_li'           => __( '' ),
				 'show_option_none'   => __( 'No categories' ),
				 'number'             => null,
				 'echo'               => 1,
				 'depth'              => 0,
				 'current_category'   => 0,
				 'pad_counts'         => 0,
				 'taxonomy'           => 'category',
				 'walker'             => null,
				 );
				 echo '<ul class="cat-parent">';
				 wp_list_categories( $args );
				 echo '</ul>';*/
			?>

            <div id="pre-owned" class="campaigns-holder">
                <?php
                    $newCatory = customGetChildren();
                    foreach($newCatory as $content){
                        //  print("<pre>".print_r($content,true)."</pre>");
                        ?>
                        <div class="campaign">
                        <?php
                        if ($content['cat_id'] == 108){
                            // echo $content['cat_id'];
                            ?>

                                <div class="parent-block-<?php echo $content['cat_id']; ?>">
                                    <!--<img src="<?php /*echo get_template_directory_uri();*/?>/images/default.png">-->
                                    <p class="cat-name">New Models<?php //echo $content['cat_name']; ?></p>
                                </div>

                                <div class="category block-<?php echo $content['cat_id']; ?>">
                                    <div class="owl-carousel carousel">
                                        <?php
                                        // revese the array to show the latest first
                                            $reverseArreyNewModels = $content['child_elements'];
                                            krsort($reverseArreyNewModels);
                                            foreach($reverseArreyNewModels as $content_child){
                                            // foreach($content['child_elements'] as $content_child){
                                                $category_extra_fields=get_option( "category_".$content_child->term_id);
                                                // print_r($category_extra_fields);//die();
                                                $category_date="";
                                                if(isset($category_extra_fields['extra3']))
                                                {
                                                    $category_date=$category_extra_fields['extra3'];
                                                }
                                                $category_model="";
                                                    if(isset($category_extra_fields['extra1']))
                                                    {
                                                        $category_model=$category_extra_fields['extra1'];
                                                    }


                                                    if ($category_model == "New Model"){

                                                ?>
													<?php //$abedDate = get_campaigns_date($content_child->term_id);
														//echo $abedDate . 'pele';
														?>

																				<?php
														$dbDate = get_campaigns_date_plain($content_child->term_id);
															if(strtotime($dbDate)>strtotime('-3 year')){  
																?>
                                                    <div class="inner-cont">
                                                        <!-- <p class="copy-page">Page</p> -->
                                                        <!-- <img src=" -->
                                                        <?php // echo get_template_directory_uri();?>
                                                        <!-- /images/default.png"> -->
                                                        <?php //if (function_exists('z_taxonomy_image')) z_taxonomy_image($content_child->term_id); ?>
                                                        <a style="background-image: url(' <?php if (function_exists('z_taxonomy_image_url')) echo z_taxonomy_image_url($content_child->term_id); ?> ')" href="<?php echo $content_child->taxonomy . '/' . $content_child->slug?>">
                                                            <img src="<?php  if (function_exists('z_taxonomy_image_url')) echo z_taxonomy_image_url($content_child->term_id); ?>" >
                                                         </a>
                                                        <a href="<?php echo $content_child->taxonomy . '/' . $content_child->slug?>"><?php echo $content_child->name;?></a>
                                                        <!-- <p class="copy-category">Category</p> -->
                                                        <?php //echo $content_child->term_id;?>
                                                       <?php get_campaigns_date($content_child->term_id);?>
										<?php
														$dbDate = get_campaigns_date_plain($content_child->term_id);
														if(strtotime($dbDate)<strtotime('-3 year')){
														//	echo "YES";
														}else{
														//	echo "NOP";
														}
										?>														
														

                                                        <!-- <p class="copy-date">Date: <strong><?php //echo $category_date;?><?php //get_campaigns_date($content_child->term_id);?></strong></p> -->
                                                        <!-- <p class="copy-date">Model: <strong><?php //echo $category_model;?></strong></p> -->

                                                    </div>
											
                                                <?php
																}
                                                    }
                                            }
                                        ?>
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                        <?php
                    }

                ?>
            </div>

            <div class="campaigns-holder">
                <?php
                    $newCatory = customGetChildren();
                    foreach($newCatory as $content){
                        //  print("<pre>".print_r($content,true)."</pre>");
                        ?>
                        <div class="campaign">
                        <?php
                        if ($content['cat_id'] == 108){
                            // echo $content['cat_id'];
                            ?>

                                <div class="parent-block-<?php echo $content['cat_id']; ?>">
                                    <!--<img src="<?php /*echo get_template_directory_uri();*/?>/images/default.png">-->
                                    <p class="cat-name">Pre-owned<?php //echo $content['cat_name']; ?></p>
                                </div>

                                <div class="category block-<?php echo $content['cat_id']; ?>">
                                    <div class="owl-carousel carousel">
                                        <?php
                                            // revese the array to show the latest first
                                            $reverseArreyPreOwned = $content['child_elements'];
                                            krsort($reverseArreyPreOwned);
                                            foreach($reverseArreyPreOwned as $content_child){
                                            // foreach($content['child_elements'] as $content_child){
                                                $category_extra_fields=get_option( "category_".$content_child->term_id);
//                                                print_r($category_extra_fields);die();
                                                $category_date="";
                                                if(isset($category_extra_fields['extra3']))
                                                {
                                                    $category_date=$category_extra_fields['extra3'];
                                                }
                                                $category_model="";
                                                    if(isset($category_extra_fields['extra1']))
                                                    {
                                                        $category_model=$category_extra_fields['extra1'];
                                                    }

                                                    if ($category_model == "Pre-Owned"){
                                                ?>
										<?php
														$dbDate = get_campaigns_date_plain($content_child->term_id);
															if(strtotime($dbDate)>strtotime('-3 year')){  
																?>
                                                    <div class="inner-cont">
                                                        <!-- <p class="copy-page">Page</p> -->
                                                        <!-- <img src=" -->
                                                        <?php // echo get_template_directory_uri();?>
                                                        <!-- /images/default.png"> -->
                                                        <?php //if (function_exists('z_taxonomy_image')) z_taxonomy_image($content_child->term_id); ?>
                                                        <a style="background-image: url(' <?php if (function_exists('z_taxonomy_image_url')) echo z_taxonomy_image_url($content_child->term_id); ?> ')" href="<?php echo $content_child->taxonomy . '/' . $content_child->slug?>">
                                                            <img src="<?php  if (function_exists('z_taxonomy_image_url')) echo z_taxonomy_image_url($content_child->term_id); ?>" >
                                                         </a>
                                                        <a href="<?php echo $content_child->taxonomy . '/' . $content_child->slug?>"><?php echo $content_child->name;?></a>
                                                        <!-- <p class="copy-category">Category</p> -->
                                                        <?php //echo $content_child->term_id;?>
                                                        <?php get_campaigns_date($content_child->term_id);?>
                                                        <!-- <p class="copy-date">Date: <strong><?php //echo $category_date;?></strong></p> -->
                                                        <!-- <p class="copy-date">Model: <strong><?php //echo $category_model;?></strong></p> -->

                                                    </div>
                                                <?php
															}
                                                    }
                                            }
                                        ?>
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                        <?php
                    }

                ?>
            </div>

            <!-- After Sales -->

            <!--main copy page intro-->
            <!-- <div class="intro" style="margin-top:43px;">
                <h3 class="page-tittle">After Sales</h3>
            </div> -->

            <div class="campaigns-holder">
                <?php
                    $newCatory = customGetChildren();
                    foreach($newCatory as $content){
                        // print_r($content);
                        //  print("<pre>".print_r($content,true)."</pre>");
                     
                        ?>
                        <div class="campaign">
                        <?php
                        if ($content['cat_id'] == 118 ){
                            // echo $content['cat_id'];
                            ?>
                                <div class="parent-block-<?php echo $content['cat_id']; ?>">
                                    <!--<img src="<?php /*echo get_template_directory_uri();*/?>/images/default.png">-->
                                    <p class="cat-name">After Sales Models<?php //echo $content['cat_name']; ?></p>
                                </div>
                                <div class="category block-<?php echo $content['cat_id']; ?>">
                                    <div class="owl-carousel carousel">
                                        <?php
                                            // revese the array to show the latest first
                                            $reverseArreyNewModels = $content['child_elements'];
                                            krsort($reverseArreyNewModels);
                                            foreach($reverseArreyNewModels as $content_child){
                                            // foreach($content['child_elements'] as $content_child){
                                                $category_extra_fields=get_option( "category_".$content_child->term_id);

                                                $category_date="";
                                                if(isset($category_extra_fields['extra3']))
                                                {
                                                    $category_date=$category_extra_fields['extra3'];
                                                }
                                                $category_model="";
                                                    if(isset($category_extra_fields['extra1']))
                                                    {
                                                        $category_model=$category_extra_fields['extra1'];
                                                    }


                                                    if ($category_model == "New Model"){

                                                ?>
										<?php
														$dbDate = get_campaigns_date_plain($content_child->term_id);
														if(strtotime($dbDate)>strtotime('-3 year')){  
										?>
                                                <div class="inner-cont">
                                                        <!-- <p class="copy-page">Page</p> -->
                                                        <!-- <img src=" -->
                                                        <?php // echo get_template_directory_uri();?>
                                                        <!-- /images/default.png"> -->
                                                        <?php //if (function_exists('z_taxonomy_image')) z_taxonomy_image($content_child->term_id); ?>
                                                        <a style="background-image: url(' <?php if (function_exists('z_taxonomy_image_url')) echo z_taxonomy_image_url($content_child->term_id); ?> ')" href="https://audi-dealerhub.glooatogilvy.co.za/<?php echo $content_child->taxonomy . '/' . $content_child->slug?>">
                                                            <img src="<?php  if (function_exists('z_taxonomy_image_url')) echo z_taxonomy_image_url($content_child->term_id); ?>" >
                                                         </a>
                                                        <!-- <a href="<?php //echo $content_child->taxonomy . '/' . $content_child->slug?>"><?php //echo $content_child->name;?></a> -->
                                                        <a href="https://audi-dealerhub.glooatogilvy.co.za/<?php echo $content_child->taxonomy . '/' . $content_child->slug?>"><?php echo $content_child->name;?></a>
                                                        <!-- <p class="copy-category">Category</p> -->
                                                        <?php //echo $content_child->term_id;?>
                                                        <?php get_campaigns_date($content_child->term_id);?>
                                                        <?php

															// if ($category_date) {
															// 	echo '<p class="copy-date">Date: <strong>';
															// 	echo $category_date;
															// 	echo '</strong></p>';
															// }
                                                        ?>


                                                        <!-- <p class="copy-date">Model: <strong><?php //echo $category_model;?></strong></p> -->

                                                    </div>
                                                    <?php
														}
                                                    }
                                            }
                                        ?>
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                        <?php
                    }

                ?>
            </div>


            <div class="campaigns-holder">
              <?php
                    $newCatory = customGetChildren();
                    foreach($newCatory as $content){
                        //  print("<pre>".print_r($content,true)."</pre>");
                        ?>
                        <div class="campaign">
                        <?php
                        if ($content['cat_id'] == 118 ){
                            // echo $content['cat_id'];
                            ?>

                                <div class="parent-block-<?php echo $content['cat_id']; ?>">
                                    <!--<img src="<?php /*echo get_template_directory_uri();*/?>/images/default.png">-->
                                    <p class="cat-name">After Sales Pre-owned<?php //echo $content['cat_name']; ?></p>
                                </div>

                                <div class="category block-<?php echo $content['cat_id']; ?>">
                                    <div class="owl-carousel carousel">
                                        <?php
                                            // revese the array to show the latest first
                                            $reverseArreyPreOwned = $content['child_elements'];
                                            krsort($reverseArreyPreOwned);
                                            foreach($reverseArreyPreOwned as $content_child){
                                            // foreach($content['child_elements'] as $content_child){
                                                $category_extra_fields=get_option( "category_".$content_child->term_id);
                                                $category_date="";
                                                if(isset($category_extra_fields['extra3']))
                                                {
                                                    $category_date=$category_extra_fields['extra3'];
                                                }
                                                $category_model="";
                                                    if(isset($category_extra_fields['extra1']))
                                                    {
                                                        $category_model=$category_extra_fields['extra1'];
                                                    }

                                                    if ($category_model == "Pre-Owned"){
                                                ?>
										<?php
														$dbDate = get_campaigns_date_plain($content_child->term_id);
														if(strtotime($dbDate)>strtotime('-3 year')){  
										?>
                                                    <div class="inner-cont">
                                                        <!-- <p class="copy-page">Page</p> -->
                                                        <!-- <img src=" -->
                                                        <?php // echo get_template_directory_uri();?>
                                                        <!-- /images/default.png"> -->
                                                        <?php //if (function_exists('z_taxonomy_image')) z_taxonomy_image($content_child->term_id); ?>
                                                        <a style="background-image: url(' <?php if (function_exists('z_taxonomy_image_url')) echo z_taxonomy_image_url($content_child->term_id); ?> ')" href="https://audi-dealerhub.glooatogilvy.co.za/<?php echo $content_child->taxonomy . '/' . $content_child->slug?>">
                                                            <img src="<?php  if (function_exists('z_taxonomy_image_url')) echo z_taxonomy_image_url($content_child->term_id); ?>" >
                                                         </a>
                                                    <!-- <a href="<?php //echo $content_child->taxonomy . '/' . $content_child->slug?>"><?php //echo $content_child->name;?></a> -->
                                                        <a href="https://audi-dealerhub.glooatogilvy.co.za/<?php echo $content_child->taxonomy . '/' . $content_child->slug?>"><?php echo $content_child->name;?></a>
                                                        
                                                        <!-- <p class="copy-category">Category</p> -->
                                                        <?php // echo $content_child->term_id; ?>
                                                        <?php get_campaigns_date($content_child->term_id);?>
														<?php

															// if ($category_date) {
															// 	echo '<p class="copy-date">Date: <strong>';
															// 	echo $category_date;
															// 	echo '</strong></p>';
															// }
														?>
                                                        
                                                        <!-- <p class="copy-date">Model: <strong><?php //echo $category_model;?></strong></p> -->
                                                        </div>
                                                <?php
														}
                                                    }
                                            }
                                        ?>
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                        <?php
                    }

                ?>
            </div> 







			
			
            <!--main copy page intro-->
            <div id="archive" class="intro" style="margin-top:43px;">
            <h3 class="page-tittle">Archived Campaigns</h3>
                <!-- <a href="/marketing-and-communications-new-models/">New Models</a>
                <a href="/marketing-and-communications-pre-owned/">Pre-owned</a> -->
            </div>	
			
			 <div class="campaigns-holder">
                <?php
                    $newCatory = customGetChildren();
                    foreach($newCatory as $content){
                        //  print("<pre>".print_r($content,true)."</pre>");
                        ?>
                        <div class="campaign">
                        <?php
                        if ($content['cat_id'] == 108 ){
                            // echo $content['cat_id'];
                            ?>

                                <div class="parent-block-<?php echo $content['cat_id']; ?>">
                                    <!--<img src="<?php /*echo get_template_directory_uri();*/?>/images/default.png">-->
                                    <p class="cat-name">Models<?php //echo $content['cat_name']; ?></p>
                                </div>

                                <div class="category block-<?php echo $content['cat_id']; ?>">
                                    <div class="owl-carousel carousel">
                                        <?php
                                        // revese the array to show the latest first
                                            $reverseArreyNewModels = $content['child_elements'];
                                            krsort($reverseArreyNewModels);
                                            foreach($reverseArreyNewModels as $content_child){
                                            // foreach($content['child_elements'] as $content_child){
                                                $category_extra_fields=get_option( "category_".$content_child->term_id);
                                                // print_r($category_extra_fields);//die();
                                                $category_date="";
                                                if(isset($category_extra_fields['extra3']))
                                                {
                                                    $category_date=$category_extra_fields['extra3'];
                                                }
                                                $category_model="";
                                                    if(isset($category_extra_fields['extra1']))
                                                    {
                                                        $category_model=$category_extra_fields['extra1'];
                                                    }


                                                    if ($category_model == "New Model"){

                                                ?>
													<?php //$abedDate = get_campaigns_date($content_child->term_id);
														//echo $abedDate . 'pele';
														?>
										<?php
// 														$dbDate = get_campaigns_date($content_child->term_id);
// 														if(strtotime($dbDate)<strtotime('-1 year')){
// 															echo "YES";
// 														}else{
// 															echo "NOP";
// 														}
										?>
																				<?php
														$dbDate = get_campaigns_date_plain($content_child->term_id);
															if(strtotime($dbDate)<strtotime('-3 year')){  
																?>
                                                    <div class="inner-cont">
                                                        <!-- <p class="copy-page">Page</p> -->
                                                        <!-- <img src=" -->
                                                        <?php // echo get_template_directory_uri();?>
                                                        <!-- /images/default.png"> -->
                                                        <?php //if (function_exists('z_taxonomy_image')) z_taxonomy_image($content_child->term_id); ?>
                                                        <a style="background-image: url(' <?php if (function_exists('z_taxonomy_image_url')) echo z_taxonomy_image_url($content_child->term_id); ?> ')" href="<?php echo $content_child->taxonomy . '/' . $content_child->slug?>">
                                                            <img src="<?php  if (function_exists('z_taxonomy_image_url')) echo z_taxonomy_image_url($content_child->term_id); ?>" >
                                                         </a>
                                                        <a href="<?php echo $content_child->taxonomy . '/' . $content_child->slug?>"><?php echo $content_child->name;?></a>
                                                        <!-- <p class="copy-category">Category</p> -->
                                                        <?php //echo $content_child->term_id;?>
                                                       <?php get_campaigns_date($content_child->term_id);?>
										<?php
														$dbDate = get_campaigns_date_plain($content_child->term_id);
// 														echo $dbDate;
														if(strtotime($dbDate)<strtotime('-3 year')){
// 															//echo "YES";
														}else{
															//echo "NOP";
														}
										?>														
														

                                                        <!-- <p class="copy-date">Date: <strong><?php //echo $category_date;?><?php //get_campaigns_date($content_child->term_id);?></strong></p> -->
                                                        <!-- <p class="copy-date">Model: <strong><?php //echo $category_model;?></strong></p> -->

                                                    </div>
											
                                                <?php
																}
                                                    }
                                            }
                                        ?>
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                        <?php
                    }

                ?>
            </div>
			
		<div class="campaigns-holder">
                <?php
                    $newCatory = customGetChildren();
                    foreach($newCatory as $content){
                        //  print("<pre>".print_r($content,true)."</pre>");
                        ?>
                        <div class="campaign">
                        <?php
                        if ($content['cat_id'] == 108 ){
                            // echo $content['cat_id'];
                            ?>

                                <div class="parent-block-<?php echo $content['cat_id']; ?>">
                                    <!--<img src="<?php /*echo get_template_directory_uri();*/?>/images/default.png">-->
                                    <p class="cat-name">Pre-owned<?php //echo $content['cat_name']; ?></p>
                                </div>

                                <div class="category block-<?php echo $content['cat_id']; ?>">
                                    <div class="owl-carousel carousel">
                                        <?php
                                            // revese the array to show the latest first
                                            $reverseArreyPreOwned = $content['child_elements'];
                                            krsort($reverseArreyPreOwned);
                                            foreach($reverseArreyPreOwned as $content_child){
                                            // foreach($content['child_elements'] as $content_child){
                                                $category_extra_fields=get_option( "category_".$content_child->term_id);
//                                                print_r($category_extra_fields);die();
                                                $category_date="";
                                                if(isset($category_extra_fields['extra3']))
                                                {
                                                    $category_date=$category_extra_fields['extra3'];
                                                }
                                                $category_model="";
                                                    if(isset($category_extra_fields['extra1']))
                                                    {
                                                        $category_model=$category_extra_fields['extra1'];
                                                    }

                                                    if ($category_model == "Pre-Owned"){
                                                ?>
										<?php
														$dbDate = get_campaigns_date_plain($content_child->term_id);
															if(strtotime($dbDate)<strtotime('-3 year')){  
																?>
                                                    <div class="inner-cont">
                                                        <!-- <p class="copy-page">Page</p> -->
                                                        <!-- <img src=" -->
                                                        <?php // echo get_template_directory_uri();?>
                                                        <!-- /images/default.png"> -->
                                                        <?php //if (function_exists('z_taxonomy_image')) z_taxonomy_image($content_child->term_id); ?>
                                                        <a style="background-image: url(' <?php if (function_exists('z_taxonomy_image_url')) echo z_taxonomy_image_url($content_child->term_id); ?> ')" href="<?php echo $content_child->taxonomy . '/' . $content_child->slug?>">
                                                            <img src="<?php  if (function_exists('z_taxonomy_image_url')) echo z_taxonomy_image_url($content_child->term_id); ?>" >
                                                         </a>
                                                        <a href="<?php echo $content_child->taxonomy . '/' . $content_child->slug?>"><?php echo $content_child->name;?></a>
                                                        <!-- <p class="copy-category">Category</p> -->
                                                        <?php //echo $content_child->term_id;?>
                                                        <?php get_campaigns_date($content_child->term_id);?>
                                                        <!-- <p class="copy-date">Date: <strong><?php //echo $category_date;?></strong></p> -->
                                                        <!-- <p class="copy-date">Model: <strong><?php //echo $category_model;?></strong></p> -->

                                                    </div>
                                                <?php
															}
                                                    }
                                            }
                                        ?>
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                        <?php
                    }

                ?>
            </div>
			
						            <!--main copy page intro-->
                                    <!-- <div class="intro" style="margin-top:43px;">
            <h3 class="page-tittle">After Sales (Archived)</h3>
              
            </div> -->
			
			<div class="campaigns-holder">
                <?php
                    $newCatory = customGetChildren();
                    foreach($newCatory as $content){
                        // print_r($content);
                        //  print("<pre>".print_r($content,true)."</pre>");
                     
                        ?>
                        <div class="campaign">
                        <?php
                        if ($content['cat_id'] == 118 ){
                            // echo $content['cat_id'];
                            ?>
                                <div class="parent-block-<?php echo $content['cat_id']; ?>">
                                    <!--<img src="<?php /*echo get_template_directory_uri();*/?>/images/default.png">-->
                                    <p class="cat-name">After Sales Models<?php //echo $content['cat_name']; ?></p>
                                </div>
                                <div class="category block-<?php echo $content['cat_id']; ?>">
                                    <div class="owl-carousel carousel">
                                        <?php
                                            // revese the array to show the latest first
                                            $reverseArreyNewModels = $content['child_elements'];
                                            krsort($reverseArreyNewModels);
                                            foreach($reverseArreyNewModels as $content_child){
                                            // foreach($content['child_elements'] as $content_child){
                                                $category_extra_fields=get_option( "category_".$content_child->term_id);
                                                $category_date="";
                                                if(isset($category_extra_fields['extra3']))
                                                {
                                                    $category_date=$category_extra_fields['extra3'];
                                                }
                                                $category_model="";
                                                    if(isset($category_extra_fields['extra1']))
                                                    {
                                                        $category_model=$category_extra_fields['extra1'];
                                                    }


                                                    if ($category_model == "New Model"){

                                                ?>
										<?php
														$dbDate = get_campaigns_date_plain($content_child->term_id);
														if(strtotime($dbDate)<strtotime('-3 year')){  
										?>
                                                <div class="inner-cont">
                                                        <!-- <p class="copy-page">Page</p> -->
                                                        <!-- <img src=" -->
                                                        <?php // echo get_template_directory_uri();?>
                                                        <!-- /images/default.png"> -->
                                                        <?php //if (function_exists('z_taxonomy_image')) z_taxonomy_image($content_child->term_id); ?>
                                                        <a style="background-image: url(' <?php if (function_exists('z_taxonomy_image_url')) echo z_taxonomy_image_url($content_child->term_id); ?> ')" href="https://audi-dealerhub.glooatogilvy.co.za/<?php echo $content_child->taxonomy . '/' . $content_child->slug?>">
                                                            <img src="<?php  if (function_exists('z_taxonomy_image_url')) echo z_taxonomy_image_url($content_child->term_id); ?>" >
                                                         </a>
                                                        <!-- <a href="<?php //echo $content_child->taxonomy . '/' . $content_child->slug?>"><?php //echo $content_child->name;?></a> -->
                                                        <a href="https://audi-dealerhub.glooatogilvy.co.za/<?php echo $content_child->taxonomy . '/' . $content_child->slug?>"><?php echo $content_child->name;?></a>
                                                        <!-- <p class="copy-category">Category</p> -->
                                                        <?php //echo $content_child->term_id;?>
                                                        <?php get_campaigns_date($content_child->term_id);?>
                                                        <?php

															// if ($category_date) {
															// 	echo '<p class="copy-date">Date: <strong>';
															// 	echo $category_date;
															// 	echo '</strong></p>';
															// }
                                                        ?>


                                                        <!-- <p class="copy-date">Model: <strong><?php //echo $category_model;?></strong></p> -->

                                                    </div>
                                                    <?php
														}
                                                    }
                                            }
                                        ?>
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                        <?php
                    }

                ?>
            </div>
			
			
			
			
			
			
			
			
			




        
        </div>
    </div>
    </body>
    </html>

<?php get_footer();?>