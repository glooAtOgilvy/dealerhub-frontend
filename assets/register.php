<?php /* Template Name: Register */ ?>


<?php get_header();?>

<div class="copy-container registration">
    <div class="reg-copy-container">
        <p>Please submit the form to apply for access to the Dealer Hub.
            <br>
            Your application can take up to 48 hours to approve.
        </p>
    </div>
    <div class="registration-form">

		<?php

			while ( have_posts() ) :
				the_post();
				get_template_part( 'template-parts/content/content-page-register' );

				// If comments are open or there is at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) {
					comments_template();
				}
			endwhile; // End of the loop.
		?>

    </div>
</div>

<?php get_footer();?>
