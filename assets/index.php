<?php get_header(); ?>
<?php 
        $catergory = get_the_category();
        $posts = get_posts(
            array(
            	'posts_per_page' => '25',
                'orderby' => 'menu_order',
                'category_name' => $catergory[0]->slug,
                'order' => 'ASC'
            )
        );
    ?>
<div id="page" class="content-index">

<div class="container">

    <div class="col-xs-3 pull-right site-logo">
    	<img src="<?php echo get_template_directory_uri();?>/images/logo.svg">
    </div>

</div>

<div class="<?php echo $catergory[0]->slug;?> hero">
    <img src="<?php echo get_template_directory_uri();?>/images/blank.png" style="visibility: hidden!important;">
</div>
<div class="outer-container">

    <?php get_sidebar(); ?>
    <div class="main-copy-holder content-copy">

<!--<nav id="access" role="navigation">
<a class="open-panel"><span>open</span></a>
<a class="close-panel"><span>close</span></a>-->
        <div class="page-menu">
            <?php /* Choose section menu depending on value of Custom Fields key: "section" */
                $sectmenu = "section1"; //set a defult in case we find nothing
                    $sectmenu = get_post_meta($post->ID, 'Position', true);
                    switch ($sectmenu):
                        case "1":
                            wp_nav_menu( array(
                            	'theme_location' => 'section1',
                            	'container_class' => 'off-canvas-menu'
                            ));
                            break;
                        case "2":
                            wp_nav_menu( array( 
                            	'theme_location' => 'section2',
                            	 'container_class' => 'off-canvas-menu'
                            	));
                            break;
                        case "3":
                            wp_nav_menu( array( 
                            	'theme_location' => 'section3',
                            	'container_class' => 'off-canvas-menu'
                            	));
                            break;
                        case "4":
                            wp_nav_menu( array( 
                                'theme_location' => 'section4',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "5":
                            wp_nav_menu( array( 
                                'theme_location' => 'section5',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
						case "6":
                            wp_nav_menu( array( 
                                'theme_location' => 'section6',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
						case "7":
                            wp_nav_menu( array( 
                                'theme_location' => 'section7',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
						case "8":
                            wp_nav_menu( array( 
                                'theme_location' => 'section8',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
						case "9":
                            wp_nav_menu( array( 
                                'theme_location' => 'section9',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
						case "10":
                            wp_nav_menu( array( 
                                'theme_location' => 'section10',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "11":
                            wp_nav_menu( array( 
                                'theme_location' => 'section11',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "12":
                            wp_nav_menu( array( 
                                'theme_location' => 'section12',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
						case "13":
                            wp_nav_menu( array( 
                                'theme_location' => 'section13',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
						case "14":
                            wp_nav_menu( array( 
                                'theme_location' => 'section14',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;                            
						case "15":
                            wp_nav_menu( array( 
                                'theme_location' => 'section15',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
						case "16":
                            wp_nav_menu( array( 
                                'theme_location' => 'section16',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
						case "17":
                            wp_nav_menu( array( 
                                'theme_location' => 'section17',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
						case "18":
                            wp_nav_menu( array( 
                                'theme_location' => 'section18',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
						case "19":
                            wp_nav_menu( array( 
                                'theme_location' => 'section19',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
						case "20":
                            wp_nav_menu( array( 
                                'theme_location' => 'section20',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "21":
                            wp_nav_menu( array( 
                                'theme_location' => 'section21',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "22":
                            wp_nav_menu( array( 
                                'theme_location' => 'section22',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "23":
                            wp_nav_menu( array( 
                                'theme_location' => 'section23',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "24":
                            wp_nav_menu( array( 
                                'theme_location' => 'section24',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "25":
                            wp_nav_menu( array( 
                                'theme_location' => 'section25',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "26":
                            wp_nav_menu( array( 
                                'theme_location' => 'section26',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "27":
                            wp_nav_menu( array( 
                                'theme_location' => 'section27',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "28":
                            wp_nav_menu( array( 
                                'theme_location' => 'section28',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "29":
                            wp_nav_menu( array( 
                                'theme_location' => 'section29',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "30":
                            wp_nav_menu( array( 
                                'theme_location' => 'section30',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "31":
                            wp_nav_menu( array( 
                                'theme_location' => 'section31',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "32":
                            wp_nav_menu( array( 
                                'theme_location' => 'section32',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "33":
                            wp_nav_menu( array( 
                                'theme_location' => 'section33',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "34":
                            wp_nav_menu( array( 
                                'theme_location' => 'section34',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "35":
                            wp_nav_menu( array( 
                                'theme_location' => 'section35',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "36":
                            wp_nav_menu( array( 
                                'theme_location' => 'section36',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "37":
                            wp_nav_menu( array( 
                                'theme_location' => 'section37',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "38":
                            wp_nav_menu( array( 
                                'theme_location' => 'section38',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "39":
                            wp_nav_menu( array( 
                                'theme_location' => 'section39',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "40":
                            wp_nav_menu( array( 
                                'theme_location' => 'section40',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "41":
                            wp_nav_menu( array( 
                                'theme_location' => 'section41',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "42":
                            wp_nav_menu( array( 
                                'theme_location' => 'section42',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "43":
                            wp_nav_menu( array( 
                                'theme_location' => 'section43',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "44":
                            wp_nav_menu( array( 
                                'theme_location' => 'section44',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "45":
                            wp_nav_menu( array( 
                                'theme_location' => 'section45',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "46":
                            wp_nav_menu( array( 
                                'theme_location' => 'section46',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "47":
                            wp_nav_menu( array( 
                                'theme_location' => 'section47',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "48":
                            wp_nav_menu( array( 
                                'theme_location' => 'section48',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "49":
                            wp_nav_menu( array( 
                                'theme_location' => 'section49',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "50":
                            wp_nav_menu( array( 
                                'theme_location' => 'section50',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "51":
                            wp_nav_menu( array( 
                                'theme_location' => 'section51',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "52":
                            wp_nav_menu( array( 
                                'theme_location' => 'section52',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "53":
                            wp_nav_menu( array( 
                                'theme_location' => 'section53',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "54":
                            wp_nav_menu( array( 
                                'theme_location' => 'section54',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "55":
                            wp_nav_menu( array( 
                                'theme_location' => 'section55',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "56":
                            wp_nav_menu( array( 
                                'theme_location' => 'section56',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "57":
                            wp_nav_menu( array( 
                                'theme_location' => 'section57',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "58":
                            wp_nav_menu( array( 
                                'theme_location' => 'section58',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "59":
                            wp_nav_menu( array( 
                                'theme_location' => 'section59',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "60":
                            wp_nav_menu( array( 
                                'theme_location' => 'section60',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "61":
                            wp_nav_menu( array( 
                                'theme_location' => 'section61',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "62":
                            wp_nav_menu( array( 
                                'theme_location' => 'section62',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "63":
                            wp_nav_menu( array( 
                                'theme_location' => 'section63',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "64":
                            wp_nav_menu( array( 
                                'theme_location' => 'section64',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "65":
                            wp_nav_menu( array( 
                                'theme_location' => 'section65',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "66":
                            wp_nav_menu( array( 
                                'theme_location' => 'section66',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "67":
                            wp_nav_menu( array( 
                                'theme_location' => 'section67',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "68":
                            wp_nav_menu( array( 
                                'theme_location' => 'section68',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "69":
                            wp_nav_menu( array( 
                                'theme_location' => 'section69',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "70":
                            wp_nav_menu( array( 
                                'theme_location' => 'section70',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "71":
                            wp_nav_menu( array( 
                                'theme_location' => 'section71',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "72":
                            wp_nav_menu( array( 
                                'theme_location' => 'section72',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "73":
                            wp_nav_menu( array( 
                                'theme_location' => 'section73',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "74":
                            wp_nav_menu( array( 
                                'theme_location' => 'section74',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "75":
                            wp_nav_menu( array( 
                                'theme_location' => 'section75',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "76":
                            wp_nav_menu( array( 
                                'theme_location' => 'section76',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "77":
                            wp_nav_menu( array( 
                                'theme_location' => 'section77',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "78":
                            wp_nav_menu( array( 
                                'theme_location' => 'section78',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "79":
                            wp_nav_menu( array( 
                                'theme_location' => 'section79',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "80":
                            wp_nav_menu( array( 
                                'theme_location' => 'section80',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "81":
                            wp_nav_menu( array( 
                                'theme_location' => 'section81',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "82":
                            wp_nav_menu( array( 
                                'theme_location' => 'section82',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "83":
                            wp_nav_menu( array( 
                                'theme_location' => 'section83',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "84":
                            wp_nav_menu( array( 
                                'theme_location' => 'section84',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "85":
                            wp_nav_menu( array( 
                                'theme_location' => 'section85',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "86":
                            wp_nav_menu( array( 
                                'theme_location' => 'section86',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "87":
                            wp_nav_menu( array( 
                                'theme_location' => 'section87',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "88":
                            wp_nav_menu( array( 
                                'theme_location' => 'section88',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "89":
                            wp_nav_menu( array( 
                                'theme_location' => 'section89',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "90":
                            wp_nav_menu( array( 
                                'theme_location' => 'section90',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "91":
                            wp_nav_menu( array( 
                                'theme_location' => 'section91',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "92":
                            wp_nav_menu( array( 
                                'theme_location' => 'section92',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "93":
                            wp_nav_menu( array( 
                                'theme_location' => 'section93',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "94":
                            wp_nav_menu( array( 
                                'theme_location' => 'section94',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "95":
                            wp_nav_menu( array( 
                                'theme_location' => 'section95',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "96":
                            wp_nav_menu( array( 
                                'theme_location' => 'section96',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "97":
                            wp_nav_menu( array( 
                                'theme_location' => 'section97',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "98":
                            wp_nav_menu( array( 
                                'theme_location' => 'section98',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "99":
                            wp_nav_menu( array( 
                                'theme_location' => 'section99',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "100":
                            wp_nav_menu( array( 
                                'theme_location' => 'section100',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "101":
                            wp_nav_menu( array( 
                                'theme_location' => 'section101',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "102":
                            wp_nav_menu( array( 
                                'theme_location' => 'section102',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "103":
                            wp_nav_menu( array( 
                                'theme_location' => 'section103',
                                'container_class' => 'off-canvas-menu'
                            ));
                        break;
                        case "104":
                            wp_nav_menu( array( 
                                'theme_location' => 'section104',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "105":
                            wp_nav_menu( array( 
                                'theme_location' => 'section105',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "106":
                            wp_nav_menu( array( 
                                'theme_location' => 'section106',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "107":
                            wp_nav_menu( array( 
                                'theme_location' => 'section107',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "108":
                            wp_nav_menu( array( 
                                'theme_location' => 'section108',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "109":
                            wp_nav_menu( array( 
                                'theme_location' => 'section109',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "110":
                            wp_nav_menu( array( 
                                'theme_location' => 'section110',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "111":
                            wp_nav_menu( array( 
                                'theme_location' => 'section111',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "112":
                            wp_nav_menu( array( 
                                'theme_location' => 'section112',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "113":
                            wp_nav_menu( array( 
                                'theme_location' => 'section113',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "114":
                            wp_nav_menu( array( 
                                'theme_location' => 'section114',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "115":
                            wp_nav_menu( array( 
                                'theme_location' => 'section115',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "116":
                            wp_nav_menu( array( 
                                'theme_location' => 'section116',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "117":
                            wp_nav_menu( array( 
                                'theme_location' => 'section117',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "118":
                            wp_nav_menu( array( 
                                'theme_location' => 'section118',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "119":
                            wp_nav_menu( array( 
                                'theme_location' => 'section119',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "120":
                            wp_nav_menu( array( 
                                'theme_location' => 'section120',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "121":
                            wp_nav_menu( array( 
                                'theme_location' => 'section121',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "122":
                            wp_nav_menu( array( 
                                'theme_location' => 'section122',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "123":
                            wp_nav_menu( array( 
                                'theme_location' => 'section123',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "124":
                            wp_nav_menu( array( 
                                'theme_location' => 'section124',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "125":
                            wp_nav_menu( array( 
                                'theme_location' => 'section125',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "126":
                            wp_nav_menu( array( 
                                'theme_location' => 'section126',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "127":
                            wp_nav_menu( array( 
                                'theme_location' => 'section127',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "128":
                            wp_nav_menu( array( 
                                'theme_location' => 'section128',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "129":
                            wp_nav_menu( array( 
                                'theme_location' => 'section129',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "130":
                            wp_nav_menu( array( 
                                'theme_location' => 'section130',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "131":
                            wp_nav_menu( array( 
                                'theme_location' => 'section131',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "132":
                            wp_nav_menu( array( 
                                'theme_location' => 'section132',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "133":
                            wp_nav_menu( array( 
                                'theme_location' => 'section133',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "134":
                            wp_nav_menu( array( 
                                'theme_location' => 'section134',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "135":
                            wp_nav_menu( array( 
                                'theme_location' => 'section135',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "136":
                            wp_nav_menu( array( 
                                'theme_location' => 'section136',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "137":
                            wp_nav_menu( array( 
                                'theme_location' => 'section137',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "138":
                            wp_nav_menu( array( 
                                'theme_location' => 'section138',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "139":
                            wp_nav_menu( array( 
                                'theme_location' => 'section139',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "140":
                            wp_nav_menu( array( 
                                'theme_location' => 'section140',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "141":
                            wp_nav_menu( array( 
                                'theme_location' => 'section141',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "142":
                            wp_nav_menu( array( 
                                'theme_location' => 'section142',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "143":
                            wp_nav_menu( array( 
                                'theme_location' => 'section143',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "144":
                            wp_nav_menu( array( 
                                'theme_location' => 'section144',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "145":
                            wp_nav_menu( array( 
                                'theme_location' => 'section145',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "146":
                            wp_nav_menu( array( 
                                'theme_location' => 'section146',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        case "147":
                            wp_nav_menu( array( 
                                'theme_location' => 'section147',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                             case "148":
                            wp_nav_menu( array( 
                                'theme_location' => 'section148',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                             case "149":
                            wp_nav_menu( array( 
                                'theme_location' => 'section149',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                             case "150":
                            wp_nav_menu( array( 
                                'theme_location' => 'section150',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                             case "151":
                            wp_nav_menu( array( 
                                'theme_location' => 'section151',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                             case "152":
                            wp_nav_menu( array( 
                                'theme_location' => 'section152',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                             case "153":
                            wp_nav_menu( array( 
                                'theme_location' => 'section153',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                             case "154":
                            wp_nav_menu( array( 
                                'theme_location' => 'section154',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                             case "155":
                            wp_nav_menu( array( 
                                'theme_location' => 'section155',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                             case "156":
                            wp_nav_menu( array( 
                                'theme_location' => 'section156',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                             case "157":
                            wp_nav_menu( array( 
                                'theme_location' => 'section157',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                             case "158":
                            wp_nav_menu( array( 
                                'theme_location' => 'section158',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                             case "159":
                            wp_nav_menu( array( 
                                'theme_location' => 'section159',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                             case "160":
                            wp_nav_menu( array( 
                                'theme_location' => 'section160',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                            case "161":
                            wp_nav_menu( array( 
                                'theme_location' => 'section161',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                            case "162":
                            wp_nav_menu( array( 
                                'theme_location' => 'section162',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                            case "163":
                            wp_nav_menu( array( 
                                'theme_location' => 'section163',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                            case "164":
                            wp_nav_menu( array( 
                                'theme_location' => 'section164',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                            case "165":
                            wp_nav_menu( array( 
                                'theme_location' => 'section165',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                            case "166":
                            wp_nav_menu( array( 
                                'theme_location' => 'section166',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                            case "167":
                            wp_nav_menu( array( 
                                'theme_location' => 'section167',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                            case "168":
                            wp_nav_menu( array( 
                                'theme_location' => 'section168',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                            case "169":
                            wp_nav_menu( array( 
                                'theme_location' => 'section169',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                            case "170":
                            wp_nav_menu( array( 
                                'theme_location' => 'section170',
                                'container_class' => 'off-canvas-menu'
                                ));
                            break;
                        default:
                            wp_nav_menu( array( 
                            	'theme_location' => 'primary',
                            	 'container_class' => 'off-canvas-menu'
                            	));
                    endswitch ;   // 'primary' exists in TwentyEleven parent ?>
    </div><!--</nav>--><!-- #access -->

        
        <?php //wp_nav_menu( array( 'theme_location' => 'header-menu') ); ?>
		        <main class="main_content clearfix">

					<?php foreach($posts as $ps) : ?>	
						<section id="<?php echo $ps->post_name;?>">
							<div id="<?php echo $ps->post_name; ?>">
								<div class="title"><h2><?php echo $ps->post_title; ?></h2></div>
								<div class="content"><?php echo wpautop($ps->post_content);?></div>
								<hr/>
							</div>
						</section>
					<?php endforeach;?>

				</main>
    </div>
</div>
<?php wp_reset_query();?>

</div><!--closes page-->

<?php get_footer();?>