jQuery(function (){
    setWidth();
    startSlider();
    menuListerners();
    loginValidation();

    if(jQuery('.copy-container.registration').length > 0){
        jQuery('body').addClass('grey-body')
    }
})

/*for any on load event functions*/
jQuery(window).on('bind', function(){
    setWidth();

    startSlider();
});

/*for any resize event functions*/
jQuery(window).on('resize', function(){
    setWidth();
});

/*for any scroll event functions*/
jQuery(window).scroll(function (event) {
    windowScrollTopPos = jQuery(window).scrollTop();

    navigationSwitchToSticky();
});

function setWidth(){
    var sideNavWidth = jQuery('.side-nav').outerWidth();
    var loginCopy = jQuery('.copy-container').outerWidth();
    var windowWidth = jQuery(window).width();
    var mainContainerWidth = jQuery(window).width() - sideNavWidth;
    var loginContainerWidth = jQuery(window).width() - loginCopy;

    if( jQuery('.login-container .copy-container').css('height') > jQuery('.graphic-container').css('height')){
        jQuery('.graphic-container').css({height:  "auto" });
    }

    jQuery('.graphic-container').css({width: loginContainerWidth + "px" });

    if(windowWidth < 780){
        jQuery('.main-copy-holder').css({width: "100%" });
    }else{
        jQuery('.main-copy-holder').css({width: (windowWidth - sideNavWidth)});
    }

    var padd = jQuery('.main-copy-holder').css('padding-left');
    jQuery('.side-nav .nav-btns.open-btn').css({ right: - (jQuery('.side-nav .nav-btns.open-btn').outerWidth() + parseInt(padd))});

    if( jQuery(".main-copy-holder").hasClass('content-copy')){
        jQuery('.side-nav .nav-btns.open-btn').css({top: '10px'});
    }

}

function startSlider(){
    jQuery('.owl-carousel.carousel').each(function(){
      jQuery(this).owlCarousel({
          loop: false,
          margin: 0,
          nav: true,
          items: 7,
          responsive:{
              0:{
                  stagePadding: 30,
                  margin: 20,
                  nav: false,
                  autoHeight: true,
                  items: 1
              },
              801:{
                  margin: 20,
                  items: 2,
                  autoHeight: true
              },
              900:{
                  margin: 20,
                  items: 4
              },
              1000:{
                  margin: 20,
                  items: 5
              },
              1366:{
                  items: 6
              }
          }
      })
    })
}

function menuListerners(){
    var html = '<div class="close-menu" id="close-menu"></div>';

    jQuery('.nav-btns').on('click',function(e){
        e.preventDefault();

        if(jQuery(this).hasClass('open-btn')){

            jQuery('.side-nav').addClass('open');
            jQuery('.outer-container').append(html);
            haltWebsiteScroll(true);

        }else{
            sidenavBye('#close-menu');
            jQuery('.side-nav').removeClass('open');
            haltWebsiteScroll(false);

        }
    })

    jQuery('.sub-nav a').on('click',function(e){

        if(jQuery(this).attr('href') === ""){
            e.preventDefault();
        }
    });

    jQuery('.sub-nav').on('click',function(e){

        if(jQuery(this).hasClass('open-nav-item')){

            jQuery('.sub-nav').removeClass('open-nav-item');
        }else{

            jQuery('.sub-nav').removeClass('open-nav-item');
            jQuery(this).addClass('open-nav-item');
        }
    });

    jQuery('.outer-container').on('click', '#close-menu', function() {
        sidenavBye();
        haltWebsiteScroll(false);
    });

    jQuery('.right-aside-column .content').each(function(){
        var eachContentScrollPosition = jQuery(this).offset().top - containerheight;
        var data = jQuery(this).attr('data-link-to-content');

        eachContentScrollPosition = parseInt(eachContentScrollPosition);

        jQuery('.left-aside-column .content-cont').each(function(){
            if(jQuery(this).find('button').attr('data-link-to-content') === data){
                jQuery(this).find('button').attr('data-scroll-position', eachContentScrollPosition);
            }
        })

        jQuery(this).attr('data-scroll-position', parseInt(eachContentScrollPosition));
    });
}

/*to do check if nav should be sticky if true then change it*/
function navigationSwitchToSticky(windowScrollTopPos){
    var navPosition = jQuery('.side-nav').scrollTop();
    var position = windowScrollTopPos + 50;

    console.log(navPosition + " : navPosition" );
    console.log(windowScrollTopPos + " : windowScrollTopPos" );
    console.log(position + " : position" );

/*
    if(position > navPosition){
        var navHeight = nav.outerHeight();
        var BannerHeight = jQuery('.banner-carousel').outerHeight() - navHeight;

        jQuery('.navigation').addClass('home');

        if (windowScrollTopPos >= BannerHeight){
            nav.addClass('sticky')
        }else{
            nav.removeClass('sticky')
        }

    }else{
        jQuery('.navigation').addClass('sticky');

        if(windowWidth >= 1200){
            jQuery('body').css('top', position);
        }else{
            jQuery('body').css('top', 0);
        }
    }*/
}


function sidenavBye() {

    if(jQuery('.side-nav').hasClass('open')){
        /*jQuery('.side-nav').css({
            'overflow-y': 'hidden'
        });*/

        jQuery('.side-nav').removeClass('open');
        jQuery('.outer-container').find('.close-menu').remove();
    }
}

function loginValidation(){
    /*var btn = jQuery('#wp-submit');
    var input = jQuery('.inp input');
    var state = false;

    btn.on('click', function(){

        jQuery('.input-container.checkmark').each(function(){
            if (jQuery(this).find('.inp input').checked !== true){
                jQuery(this).addClass('errorState')
            }
        })

    })




    jQuery('.input-container.checkmark').each(function(){
        if(jQuery(this).hasClass('errorState')){
            jQuery(this).removeClass('errorState')
        }
    })


    jQuery('.input-container.checkmark .inp input').on('click', function(){

        if (jQuery(this).parent().parent().hasClass('errorState')){
            jQuery(this).parent().parent().removeClass('errorState')
        }

    })*/

    var loginForm = jQuery('#loginform');
    var loginFormChebox = jQuery('#loginform .checkmark input[type="checkbox"]');


    for (var i = 0; i < loginFormChebox.length; i++){
        loginFormChebox.attr('data-check', 'none');
        console.log("test");
    }

    jQuery('#wp-submit').on('click',function(){
        for (var i = 0; i < loginFormChebox.length; i++){
            if(jQuery(loginFormChebox[i]).attr('data-check') === "none"){
                jQuery(loginFormChebox[i]).parent().addClass('errorState');
            }

        }
    });

    loginFormChebox.on('click',function(){
        if(jQuery(this).attr('data-check') === "none"){
            jQuery(this).attr('data-check', "checked")
            jQuery(this).parent().removeClass('errorState');
        }else{
            jQuery(this).attr('data-check', "none")
            jQuery(this).parent().addClass('errorState');
        }
    });
}

/*this is to halt the scrolling for the website*/
function haltWebsiteScroll(condition){
    if(condition){
        jQuery('body').css({
            'margin': 0,
            'height': '100%',
            'overflow-y': 'hidden'
        })
    }else{
        jQuery('body').css({
            'height': 'auto',
            'overflow-y': 'auto'
        })
    }
}