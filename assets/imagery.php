<?php
/* Template Name: Imagery Template */

?>




<style>
    div.gallery {
        margin: 5px;
        border: 1px solid #ccc;
        float: left;
        width: 180px;
    }

    div.gallery:hover {
        border: 1px solid #777;
    }

    div.gallery img {
        width: 100%;
        height: auto;
    }

    div.desc {
        padding: 15px;
        text-align: center;
    }
</style>


<?php get_header();?>

    <!DOCTYPE html>
    <html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
    </head>
    <body>

    <div class="outer-container">
        <?php get_sidebar(); ?>
        <!-- <div class="side-nav">
            <ul>
                <li class="nav-item">
                    <h2>Dealer Hub</h2>
                </li>
                <li class="nav-item sub-nav">
                    <a href="">Recently added</a>
                </li>
                <li class="nav-item sub-nav">
                    <a href="/campaigns">Campaigns</a>
                </li>
                <li class="nav-item sub-nav">
                    <a href="/after-sales">After Sales</a>
                </li>
                <li class="nav-item sub-nav">
                    <a href="/images">Images</a>
                </li>
                <li class="nav-item sub-nav">
                    <a href="advertising-guidelines">Advertising guidelines</a>
                </li>
                <li class="nav-item sub-nav">
                    <a href="additional-resources">Additional resources</a>
                </li>
                <li class="nav-item sub-nav">
                    <a href="/subscribe">Subscribe</a>
                </li>
                <li class="nav-item sub-nav">
                    <a href="/wp-login.php?action=logout">Logout</a>
                </li>
            </ul>
        </div> -->
        <div class="main-copy-holder">
            <!--search bar-->
            <div class="search-bar-holder">
                <div class="search-bar">
                    <?php
                        while (have_posts()) :
                            the_post();
                            get_template_part('template-parts/content/content-page-search');
                        endwhile;
                    ?>
                </div>
            </div>
            <!--search bar end-->

            <!--main copy page intro-->
            <div class="intro">
                <h3 class="page-tittle">Campaigns</h3>
                <a href="">New Models</a>
                <a href="">Pre-owned</a>
            </div>

			<?php
				/* $args = array(
				 'show_option_all'    => '',
				 'orderby'            => 'name',
				 'order'              => 'ASC',
				 'style'              => 'list',
				 'show_count'         => 0,
				 'hide_empty'         => 1,
				 'use_desc_for_title' => 1,
				 'child_of'           => 0,
				 'feed'               => '',
				 'feed_type'          => '',
				 'feed_image'         => '',
				 'exclude'            => '',
				 'exclude_tree'       => '',
				 'include'            => '',
				 'hierarchical'       => 1,
				 'title_li'           => __( '' ),
				 'show_option_none'   => __( 'No categories' ),
				 'number'             => null,
				 'echo'               => 1,
				 'depth'              => 0,
				 'current_category'   => 0,
				 'pad_counts'         => 0,
				 'taxonomy'           => 'category',
				 'walker'             => null,
				 );
				 echo '<ul class="cat-parent">';
				 wp_list_categories( $args );
				 echo '</ul>';*/
			?>

            <div class="campaigns-holder">
            <?php
$query_images_args = array(
    'posts_per_page' => '25',
    'post_type'      => 'attachment',
    'post_mime_type' => 'image',
    'post_status'    => 'inherit',
    'posts_per_page' => - 1,
);

$query_images = new WP_Query( $query_images_args );


$images = array();
foreach ( $query_images->posts as $image ) {
    $images[] = wp_get_attachment_url( $image->ID );
?>

<div class="gallery">
    <a target="_blank" href="<<?php echo wp_get_attachment_url( $image->ID ); ?>">
        <img src="<?php echo wp_get_attachment_url( $image->ID ); ?>" alt="Cinque Terre" width="600" height="400">

    </a>
    <div class="desc"><?php echo trim(strip_tags( $image->post_title )); ?></div>
</div>
<?php
}
?>
            </div>
        </div>
    </div>
    </body>
    </html>

<?php get_footer();?>










