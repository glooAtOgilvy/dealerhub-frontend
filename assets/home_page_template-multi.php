<?php
/* Template Name: Home Page Template */

?>




<?php get_header();?>

    <!DOCTYPE html>
    <html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <style>
            * {
                box-sizing: border-box;
            }

            /* Create two unequal columns that floats next to each other */
            .column {
                float: left;
                padding: 10px;
                height: 100%; /* Should be removed. Only for demonstration */
            }

            /* Clear floats after the columns */
            .row:after {
                content: "";
                display: table;
                clear: both;
            }
            .cat-child-temp {
                padding-left: 0;
                margin-left: 0;
            }

        </style>
    </head>
    <body>

    <div class="outer-container">
        <div class="side-nav">
            <ul>
                <li class="nav-item">
                    <h2>Dealer Hub</h2>
                </li>
                <li class="nav-item sub-nav">
                    <a href="">Recently added</a>
                </li>
                <li class="nav-item sub-nav">
                    <a href="/campaigns">Campaigns</a>
                </li>
                <li class="nav-item sub-nav">
                    <a href="/after-sales">After Sales</a>
                </li>
                <li class="nav-item sub-nav">
                    <a href="">Images</a>
                </li>
                <li class="nav-item sub-nav">
                    <a href="advertising-guidelines">Advertising guidelines</a>
                </li>
                <li class="nav-item sub-nav">
                    <a href="additional-resources">Additional resources</a>
                </li>
                <li class="nav-item sub-nav">
                    <a href="/subscribe">Subscribe</a>
                </li>
                <li class="nav-item sub-nav">
                    <a href="/wp-login.php?action=logout">Logout</a>
                </li>
            </ul>
        </div>
        <div class="main-copy-holder">
            <!--search bar-->
            <div class="search-bar-holder">
                <div class="search-bar">
                    <?php
                        while (have_posts()) :
                            the_post();
                            get_template_part('template-parts/content/content-page-search');
                        endwhile;
                    ?>
                </div>
            </div>
            <!--search bar end-->

            <h3 class="page-tittle">Campaigns</h3>

			<?php
				/* $args = array(
				 'show_option_all'    => '',
				 'orderby'            => 'name',
				 'order'              => 'ASC',
				 'style'              => 'list',
				 'show_count'         => 0,
				 'hide_empty'         => 1,
				 'use_desc_for_title' => 1,
				 'child_of'           => 0,
				 'feed'               => '',
				 'feed_type'          => '',
				 'feed_image'         => '',
				 'exclude'            => '',
				 'exclude_tree'       => '',
				 'include'            => '',
				 'hierarchical'       => 1,
				 'title_li'           => __( '' ),
				 'show_option_none'   => __( 'No categories' ),
				 'number'             => null,
				 'echo'               => 1,
				 'depth'              => 0,
				 'current_category'   => 0,
				 'pad_counts'         => 0,
				 'taxonomy'           => 'category',
				 'walker'             => null,
				 );
				 echo '<ul class="cat-parent">';
				 wp_list_categories( $args );
				 echo '</ul>';*/
			?>

            <div class="campaigns-holder">

				<?php
					$newCatory = customGetChildren();
					foreach($newCatory as $content){

						?>
                            <div class="campaign">

                                <div class="parent-block-<?php echo $content['cat_id']; ?>">
                                    <!--<img src="<?php /*echo get_template_directory_uri();*/?>/images/default.png">-->
                                    <p><?php echo $content['cat_name']; ?></p>
                                </div>


                                <div class="category block-<?php echo $content['cat_id']; ?>">

                                        <?php
                                            foreach($content['child_elements'] as $content_child){
                                            ?>
                                            <div class="inner-cont">
                                                <p class="copy-page">Page</p>
                                                <img src="<?php echo get_template_directory_uri();?>/images/default.png">
                                                <a href="<?php echo $content_child->taxonomy . '/' . $content_child->slug?>"><?php echo $content_child->name;?></a>
                                                <p class="copy-category">Category</p>
                                                <p class="copy-date">Date added</p>
                                            </div>
                                            <?php
                                        }
                                    ?>

                                </div>

                            </div>
						<?php
					}

				?>
            </div>
        </div>
    </div>


    <hr>
    <hr>
    <hr>





    <div class="row">
        <div class="column left" style="background-color:#aaa;">
            <h2>Dealer Hub</h2>
            <ul class="cat-child-temp block" >
                <li class="li-cat-child">
                  <a href="">Recently added</a>
                </li>
                <li class="li-cat-child">
                    <a href="/campaigns">Campaigns</a>
                </li>
                 <li class="li-cat-child">
                    <a href="/after-sales">After Sales</a>
                </li>
                 <li class="li-cat-child">
                    <a href="">Images</a>
                </li>
                 <li class="li-cat-child">
                    <a href="advertising-guidelines">Advertising guidelines</a>
                </li>
                 <li class="li-cat-child">
                    <a href="additional-resources">Additional resources</a>
                </li>
                 <li class="li-cat-child">
                    <a href="/subscribe">Subscribe</a>
                </li>
                <li class="li-cat-child">
                    <a href="/wp-login.php?action=logout">Logout</a>
                </li>

            </ul>
        </div>
        <div class="column right" style="background-color:#fff;">
<!--            <h2>Column 2</h2>-->
            <?php

            while (have_posts()) :
                the_post();
                get_template_part('template-parts/content/content-page-search');

            endwhile;
            ?>
            <div class="index-wrapper">
                <div class="row-fluid">
                    <div class="span12">
                        <div class="home-title">
                            <h2>Campaign Index</h2>
                        </div>

                        <?php
                        /* $args = array(
                         'show_option_all'    => '',
                         'orderby'            => 'name',
                         'order'              => 'ASC',
                         'style'              => 'list',
                         'show_count'         => 0,
                         'hide_empty'         => 1,
                         'use_desc_for_title' => 1,
                         'child_of'           => 0,
                         'feed'               => '',
                         'feed_type'          => '',
                         'feed_image'         => '',
                         'exclude'            => '',
                         'exclude_tree'       => '',
                         'include'            => '',
                         'hierarchical'       => 1,
                         'title_li'           => __( '' ),
                         'show_option_none'   => __( 'No categories' ),
                         'number'             => null,
                         'echo'               => 1,
                         'depth'              => 0,
                         'current_category'   => 0,
                         'pad_counts'         => 0,
                         'taxonomy'           => 'category',
                         'walker'             => null,
                         );
                         echo '<ul class="cat-parent">';
                         wp_list_categories( $args );
                         echo '</ul>';*/
                        ?>

                        <?php
                        $newCatory = customGetChildren();

                        foreach($newCatory as $content)
                        {
                            ?>
                            <!--<div class="square" style="height:36px;width:36px;background-color:#000;float:right"></div>-->
                            <div class="parent-cat parent-block-<?php echo $content['cat_id']; ?>">
                                <img src="<?php echo get_template_directory_uri();?>/images/arrow.jpg" style="float:right">
                                <p><?php echo $content['cat_name']; ?></p>
                            </div>

                            <?php

                            ?>
                            <ul class="cat-child block-<?php echo $content['cat_id']; ?>" style="display:none">
                                <?php
                                foreach($content['child_elements'] as $content_child)
                                {
                                    ?>
                                    <li class="li-cat-child">
                                        <img src="<?php echo get_template_directory_uri();?>/images/cat-icon.jpg" style="float:right; margin:10px"><a href="<?php echo $content_child->taxonomy . '/' . $content_child->slug?>"><?php echo $content_child->name;?></a>
                                    </li>
                                    <?php
                                }
                                ?>
                            </ul>
                            <?php
                        }

                        ?>
                    </div>
                </div>
            </div><!--close index-wrapper-->
        </div>
    </div>

    </body>
    </html>

<?php get_footer();?>