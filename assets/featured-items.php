<?php
/* Template Name: Home Page Template */

?>




<?php get_header();?>

    <!DOCTYPE html>
    <html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
    </head>
    <body>

    <div class="outer-container">
        <div class="side-nav">
            <ul>
                <li class="nav-item">
                    <h2>Dealer Hub</h2>
                </li>
                <li class="nav-item sub-nav">
                    <a href="">Recently added</a>
                </li>
                <li class="nav-item sub-nav">
                    <a href="/campaigns">Campaigns</a>
                </li>
                <li class="nav-item sub-nav">
                    <a href="/after-sales">After Sales</a>
                </li>
                <li class="nav-item sub-nav">
                    <a href="">Images</a>
                </li>
                <li class="nav-item sub-nav">
                    <a href="advertising-guidelines">Advertising guidelines</a>
                </li>
                <li class="nav-item sub-nav">
                    <a href="additional-resources">Additional resources</a>
                </li>
                <li class="nav-item sub-nav">
                    <a href="/subscribe">Subscribe</a>
                </li>
                <li class="nav-item sub-nav">
                    <a href="/wp-login.php?action=logout">Logout</a>
                </li>
            </ul>
        </div>
        <div class="main-copy-holder">
            <!--search bar-->
            <div class="search-bar-holder">
                <div class="search-bar">
                    <?php
                        while (have_posts()) :
                            the_post();
                            get_template_part('template-parts/content/content-page-search');
                        endwhile;
                    ?>
                </div>
            </div>
            <!--search bar end-->

            <!--main copy page intro-->
            <div class="intro">
                <h3 class="page-tittle">Campaigns</h3>
            </div>
            <div class="featured-item-container">
                <div class="featured-item">
                    <div class="feature-copy">
                        <h4>1. Audi Corporate Identity Guide</h4>

                        <p>The Audi CI Portal is a direct, uncomplicated way for the creative handling of our brand. It offers many examples that inspire and convey the essentials – and it’s structured in such a way that you can get started right away. A living styleguide that is being extended continuously across all touch-points.</p>
                    </div>
                    <div class="feature-image">
                        <img src="" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
    </body>
    </html>

<?php get_footer();?>