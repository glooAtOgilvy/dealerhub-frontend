<?php
/* Template Name: Login Page Template */

    get_header();
?>
<!--<link rel="stylesheet" type="text/css" href="https://audi-dealerhub.glooatogilvy.co.za/audi/audi-dealerhub.glooatogilvy.co.za/wp-content/themes/audi-campaign/css/styles.css">-->
<style>
    input[type="checkbox"] {
        cursor: pointer;
    }
</style>

<div class="login-container"><!-- login-container <-- specific to this page-->
    <div class="copy-container">
        <a class="audi-logo" style="margin-left:0;" href="#"><img src="<?php echo get_template_directory_uri();?>/images/audi-logo-01.png" alt=""></a>
        <h2 class="sub-tittle">Welcome to your</h2>
        <!--<img src="<?php /*echo get_template_directory_uri();*/?>/images/arrow.jpg" style="float:right">-->
        <h1 class="entry-title tittle">Dealer Advertising Hub</h1>
        <h1 style="color:red;font-size:30px;display:none;">This is a heading</h1>
        <form name="loginform" id="loginform" action="../wp-login.php" method="post">
            <input type="hidden" name="pmpro_login_form_used" value="1" />
            
            <div class="input-container login-username">
                <label for="user_login">Username</label>
                <input type="text" required name="log" id="user_login" class="input" value="" size="20" />
            </div>
            
            <div class="input-container login-password">
                <label for="user_pass">Password</label>
                <input type="password" required name="pwd" id="user_pass" class="input" value="" size="20" />
            </div>
            
            <div class="input-container checkmark">
                <div class="inp">
                    <input required  name="link-terms" type="checkbox" />
                    <label for="link-terms">
                        I agree to my
                    </label>
                </div>
                <a target="_blank" href="https://www.audi.co.za/za/web/en/privacy-statement.html">data usage restrictions</a>
            </div>

            <div class="input-container checkmark">
                <div class="inp">
                    <input required  name="link-terms" type="checkbox" />
                    <label for="link-terms">
                        I agree to the
                    </label>
                </div>
                <a target="_blank" href="https://www.audi.co.za/za/web/en/layers/audi-terms-and-conditions.html">Terms and Conditions</a>
            </div>

            <!-- <div class="input-container checkmark">
                <div class="inp">
                    <input onClick="window.open('/wp-content/uploads/Terms-conditions.pdf','_blank')" required  name="link-terms" type="checkbox" id="link-terms" value="link" />
                    <label for="link-terms">
                        I agree to the
                    </label>
                </div>
                <a href="">Terms and Conditions</a>
            </div>
            <div class="input-container checkmark">
                <div class="inp">
                    <input onClick="window.open('/wp-content/uploads/Terms-conditions.pdf','_blank')" required  name="link-terms" type="checkbox" id="link-terms" value="link" />
                    <label for="link-terms">
                        I agree to the
                    </label>
                </div>
                <a href="">Terms and Conditions</a>
            </div> -->
            
            <div class="input-container login-remember">
                <div class="inp">
                    <input name="rememberMe" type="checkbox" id="rememberme" value="lsRememberMe" />
                    <label>
                        Remember Me
                    </label>
                </div>
            </div>
 
            <div class="btn-container login-submit">
                <input type="hidden" name="redirect_to" value="../" />
                <input type="submit" name="wp-submit" id="wp-submit" class="btn btn-grey" value="Log In" onclick="lsRememberMe()" />
                <a href="/register" class="btn btn-white btn-bordered">Create account</a>
                <a href="/password-recovery/" class="lost-password">Lost your password?</a>
            </div>
        </form>
    </div>
    <div class="graphic-container" style="background-image: url('<?php echo get_template_directory_uri();?>/images/Lifestyle1534x1536.jpg')"> <!--add image path here-->
        <!--<img src="<?php /*echo get_template_directory_uri();*/?>/images/bg-login.png" alt="">--> <!--add image path here & alt-->
    </div>
</div>





</body>
</html>

