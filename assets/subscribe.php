<?php /* Template Name: Subscribe */ ?>
<?php get_header(); ?>
<div class="outer-container">
	<div class="side-nav">
		<ul>
			<li class="nav-item">
				<h2>Dealer Hub</h2>
			</li>
			<li class="nav-item sub-nav">
				<a href="">Recently added</a>
			</li>
			<li class="nav-item sub-nav">
				<a href="/campaigns">Campaigns</a>
			</li>
			<li class="nav-item sub-nav">
				<a href="/after-sales">After Sales</a>
			</li>
			<li class="nav-item sub-nav">
				<a href="">Images</a>
			</li>
			<li class="nav-item sub-nav">
				<a href="advertising-guidelines">Advertising guidelines</a>
			</li>
			<li class="nav-item sub-nav">
				<a href="additional-resources">Additional resources</a>
			</li>
			<li class="nav-item sub-nav">
				<a href="/subscribe">Subscribe</a>
			</li>
			<li class="nav-item sub-nav">
				<a href="/wp-login.php?action=logout">Logout</a>
			</li>
		</ul>
	</div>
	<div class="main-copy-holder vertical-center">
		<div class="subscribe-container">
			<?php
				while ( have_posts() ) :
					the_post();


					get_template_part( 'template-parts/content/content-page-subscribe' );
				endwhile;
			?>

			<div class="subscribe-image-container">
				<img src="<?php echo get_template_directory_uri();?>/images/image.png">
			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>
