<?php
/* Template Name: Login Page Template */

get_header();
?>
<!--<link rel="stylesheet" type="text/css" href="https://audi-dealerhub.glooatogilvy.co.za/audi/audi-dealerhub.glooatogilvy.co.za/wp-content/themes/audi-campaign/css/styles.css">-->
<link rel="stylesheet" type="text/css" href="../css/styles.css">

<div class="container">
    <div class="copy-container"></div>
    <div class="graphic-container" style="background-image: url('images/bg-login.png')"> <!--add image path here-->
        <img src="" alt=""> <!--add image path here & alt-->
    </div>

</div>


    <a class="skip-link screen-reader-text" href="#content">Skip to content</a>
    <div id="content" class="login site-content">
        <header class="masthead">
            <div class="row">
                <div class="medium-12 columns">
                    <h2 class="sub-tittle">Welcome to your</h2>
                </div>
            </div>
            <div class="row">
                <div class="medium-12 columns">
                    <h1 class="entry-title tittle">Dealer Advertising Hub</h1>
                </div>
            </div>
        </header>
        <div class="row">
            <div id="primary" class="medium-12 columns content-area">
                <main id="main" class="site-main" role="main">
                    <article id="post-329" class="post-329 page type-page status-publish pmpro-has-access">
                        <div class="entry-content">
                            <div class="wp-block-columns">
                                <div class="wp-block-column" style="flex-basis:25%"></div>
                                <div class="wp-block-column" style="flex-basis:50%">
                                    <div class="pmpro_login_wrap">
                                        <form name="loginform" id="loginform" action="../wp-login.php" method="post">
                                            <input type="hidden" name="pmpro_login_form_used" value="1" />
                                            <p class="login-username">
                                                <label for="user_login">Username or Email Address</label>
                                                <input type="text" name="log" id="user_login" class="input" value="" size="20" />
                                            </p>
                                            <p class="login-password">
                                                <label for="user_pass">Password</label>
                                                <input type="password" name="pwd" id="user_pass" class="input" value="" size="20" />
                                            </p>

                                            <p class="login-remember"><label><input name="rememberme" type="checkbox" id="rememberme" value="forever" /> Remember Me</label></p>
                                            <p class="login-submit">
                                                <input type="submit" name="wp-submit" id="wp-submit" class="button button-primary" value="Log In" />
                                                <input type="hidden" name="redirect_to" value="../" />
                                            </p>
                                            <p class="checkmark"><label><input name="rememberme" type="checkbox" id="rememberme" value="forever" /> I agree to my data usage restrictions</label></p>
                                            <p class="checkmark"><label><input name="rememberme" type="checkbox" id="rememberme" value="forever" /> I agree to the Terms and Conditions</label></p>
                                        </form>
                                        <hr />
                                        <p class="pmpro_actions_nav">
                                            <a href="/register">Join Now</a> | <a href="/password-recovery/">Lost Password?</a>	</p> <!-- end pmpro_actions_nav -->
                                    </div> <!-- end pmpro_login_wrap -->
                                    <script>
                                        document.getElementById('user_login').focus();
                                    </script>
                                </div>
                                <div class="wp-block-column" style="flex-basis:25%"></div>
                            </div>
                        </div><!-- .entry-content -->
                    </article><!-- #post-## -->
                </main><!-- #main -->
            </div><!-- #primary -->
        </div><!-- .row -->
    </div><!-- #content -->
</div><!-- #page -->



<!-- Memberships powered by Paid Memberships Pro v2.6.2.
-->
<script type='text/javascript' src='https://www.paidmembershipspro.com/wp-content/plugins/hurrytimer/assets/js/cookie.min.js?ver=3.14.1' id='hurryt-cookie-js'></script>
<script type='text/javascript' src='https://www.paidmembershipspro.com/wp-content/plugins/hurrytimer/assets/js/jquery.countdown.min.js?ver=2.2.0' id='hurryt-countdown-js'></script>
<script type='text/javascript' id='hurrytimer-js-extra'>
/* <![CDATA[ */
var hurrytimer_ajax_object = {"ajax_url":"https:\/\/www.paidmembershipspro.com\/wp-admin\/admin-ajax.php","ajax_nonce":"775b55736d","disable_actions":"","methods":{"COOKIE":1,"IP":2,"USER_SESSION":3},"actionsOptions":{"none":1,"hide":2,"redirect":3,"stockStatus":4,"hideAddToCartButton":5,"displayMessage":6,"expire_coupon":7},"restartOptions":{"none":1,"immediately":2,"afterReload":3,"after_duration":4},"COOKIEPATH":"\/","COOKIE_DOMAIN":"","redirect_no_back":"1","expire_coupon_message":""};
/* ]]> */
</script>
<script type='text/javascript' src='https://www.paidmembershipspro.com/wp-content/plugins/hurrytimer/assets/js/hurrytimer.js?ver=2.6.3' id='hurrytimer-js'></script>
<script type='text/javascript' src='https://www.paidmembershipspro.com/wp-content/plugins/wp-syntax/js/wp-syntax.js?ver=1.1' id='wp-syntax-js-js'></script>
<script type='text/javascript' src='https://www.paidmembershipspro.com/wp-content/themes/memberlite/js/memberlite.js?ver=4.5.3' id='memberlite-script-js'></script>
<script type='text/javascript' id='gdatt-attachments-js-extra'>
/* <![CDATA[ */
var gdbbPressAttachmentsInit = {"max_files":"3","are_you_sure":"This operation is not reversible. Are you sure?"};
/* ]]> */
</script>
<script type='text/javascript' src='https://www.paidmembershipspro.com/wp-content/plugins/gd-bbpress-attachments/js/front.min.js?ver=4.2_b2420_free' id='gdatt-attachments-js'></script>
<script type='text/javascript' src='https://www.paidmembershipspro.com/wp-includes/js/wp-embed.min.js?ver=5.8.1' id='wp-embed-js'></script>

</body>
</html>

<!--
Performance optimized by W3 Total Cache. Learn more: https://www.boldgrid.com/w3-total-cache/

Page Caching using apc (Requested URI is rejected)

Served from: www.paidmembershipspro.com @ 2021-10-11 07:53:32 by W3 Total Cache
-->