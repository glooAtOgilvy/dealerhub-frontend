<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty_One
 * @since Twenty Twenty-One 1.0
 */

?>
        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

            <?php the_title( '<h1 class="subscribe-title">', '</h1>' ); ?>


            <div class="subscribe-copy-holder">
                <?php
                the_content();

                wp_link_pages(
                    array(
                        'before'   => '<nav class="page-links" aria-label="' . esc_attr__( 'Page', 'twentytwentyone' ) . '">',
                        'after'    => '</nav>',
                        /* translators: %: Page number. */
                        'pagelink' => esc_html__( 'Page %', 'twentytwentyone' ),
                    )
                );
                ?>
            </div>
            <!-- .entry-content

            <footer class="entry-footer default-max-width">

            </footer> entry-footer -->

            <?php if ( ! is_singular( 'attachment' ) ) : ?>
                <?php get_template_part( 'template-parts/post/author-bio' ); ?>
            <?php endif; ?>

        </article><!-- #post-<?php the_ID(); ?> -->




<!--<!DOCTYPE html>-->
<!--<html>-->
<!--<body>-->

<!--<h1>Always up-to-date With the newsflash</h1>-->
<!--<form name="loginform" id="loginform" action="../wp-login.php" method="post">-->
<!--    <input type="hidden" name="pmpro_login_form_used" value="1" />-->
<!--    <p class="login-username">-->
<!--        <label for="user_login">Username or Email Address</label>-->
<!--        <input type="text" name="log" id="user_login" class="input" value="" size="20" />-->
<!--    </p>-->
<!--    <p class="login-password">-->
<!--        <label for="user_pass">Password</label>-->
<!--        <input type="password" name="pwd" id="user_pass" class="input" value="" size="20" />-->
<!--    </p>-->
<!---->
<!--    <p class="login-remember"><label><input name="rememberme" type="checkbox" id="rememberme" value="forever" /> Remember Me</label></p>-->
<!--    <p class="login-submit">-->
<!--        <input type="submit" name="wp-submit" id="wp-submit" class="button button-primary" value="Log In" />-->
<!--        <input type="hidden" name="redirect_to" value="../" />-->
<!--    </p>-->
<!---->
<!---->
<!--</form>-->
<!--<form name="your-profile"  id="your-profile" action="../wp-admin/profile.php" method="post">-->
<!--    <input type="checkbox" name="vehicle1" value="Bike">-->
<!--    <input name="pods_meta_subscribe" data-name-clean="pods-meta-subscribe" data-label="Subscribe" id="pods-form-ui-pods-meta-subscribe" type="checkbox" tabindex="2" value="1">-->
<!--    <label for="pods-form-ui-pods-meta-subscribe">Sign me up</label><br>-->

<!--    <input type="submit" value="Submit">-->
<!--    <input type="submit" name="submit" id="submit" class="button button-primary" value="Subscribe">-->
<!--    <input type="hidden" name="redirect_to" value="../subscribe" />-->
<!--</form>-->

<!--</body>-->
<!--</html>-->